package spellingbee.network;
import spellingbee.client.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();
	private int points = 0;
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.
		 * For example, based on the samples in the assignment:
		 * if (inputLine.equals("getCenter")) {
		 * 	// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
		 *      return spellingBee.getCenter();
		 * }
		 * else if ( ..... )
		return null;*/
		String[] command = inputLine.split(";");
		
		if(command[0].contentEquals("getAllLetters")) {
			return spellingBee.getAllLetters();
		}
		else if(command[0].contentEquals("getCenterLetter")) {
			char letter = spellingBee.getCenterLetter();
			String strLetter = "" + letter;
			return strLetter;
		}
		else if(command[0].contentEquals("getMessage")){
			if(command.length > 1) {
				return spellingBee.getMessage(command[1]);
			}
			else {
				return "Invalid: No Word";
			}
			
		}
		else if(command[0].contentEquals("getScore")) {
			if(command.length > 1) {
				points = points + spellingBee.getPointsForWord(command[1]);
			}
			return "" + spellingBee.getScore();

		}
		else if(command[0].contentEquals("getMax")) {
			return "" + spellingBee.getMax();
		}
		
		System.out.println(command[0]);
		return "beep";
	}
}
