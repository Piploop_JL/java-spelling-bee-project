package spellingbee.network;

import spellingbee.client.*;
public class ClientApp {

	private Client client;
	
	public ClientApp() {
		this.client = new Client();
	}
	
	public Client getClient() {
		return client;
	}
}
