//Thomas Comeau 1934037, Jimmy Le 1936415
package spellingbee.client;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import spellingbee.network.*;


public class SpellingBeeGame implements ISpellingBeeGame{
	private String[] letters;
	private char center;
	private int score;
	private ArrayList<String> wordFound = new ArrayList<String>();
	private static List<String> possibleWords;
	private int max = 0;
	private String errormessage = "";
	
	public List<String> createWordsFromFile(String path) throws IOException{
		Path base = Paths.get("..\\documents\\english.txt").toAbsolutePath();
		System.out.println(base);
		List<String> list;
		list = Files.readAllLines(base);
		return list;
	}
	
	public SpellingBeeGame() {
		Random rnd = new Random();
		boolean check = false;
		letters = new String[7];
		score = 0;
		try {
			possibleWords = createWordsFromFile("documents\\english.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int rand = rnd.nextInt(possibleWords.size());
		while(!check) {
			String wor = possibleWords.get(rand);
			System.out.println(wor);
			if(wor.length() >= 7) {
				if(isUnique(wor)) {
					check = true;
					splitLetters(wor);
				}
			}
			if(rand < possibleWords.size()-2) {
				rand++;
			}
			else {
				rand = 0;
			}
		}
		int pos = rnd.nextInt(7);
		center = letters[pos].charAt(0);
		max = maxPoints();
	}
	
	public SpellingBeeGame(String letterss) {
		letters = letterss.split("");
		Random rnd = new Random();
		int pos = rnd.nextInt(7);
		center = letters[pos].charAt(0);
		score = 0;
		try {
			possibleWords = createWordsFromFile("documents\\english.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		max = maxPoints();
	}
	
	@Override
	public int getPointsForWord(String attempt) {
		int wordpoint = 0;
		if(isValid(attempt)) {
			if (attempt.length() == 4) {
				wordpoint = 1;
			}
			else if(attempt.length() > 4) {
				wordpoint = attempt.length();	
			}
			
			if(this.isPangram(attempt)) {
				wordpoint = wordpoint + 7;	
			}
			addWord(attempt);
		}
		score = score + wordpoint;
		return wordpoint;
	}

	@Override
	public String getMessage(String attempt) {
		if(this.isValid(attempt)) {
			
			return "Great!";
		}
		else {
			return errormessage;
		}

	}

	@Override
	public String getAllLetters() {
		// TODO Auto-generated method stub
		String allletters = letters[0] + letters[1] + letters[2] +letters[3] + letters[4] + letters[5] + letters[6];
		return allletters;
	}

	@Override
	public char getCenterLetter() {
		// TODO Auto-generated method stub
		return center;
	}

	@Override
	public int getScore() {
		// TODO Auto-generated method stub
		return score;
	}

	@Override
	public int[] getBrackets() {
		int[] bracket = new int[5];
		bracket[0] = max/4;
		bracket[1] = max/2;
		bracket[2] = (int) (max * 0.75);
		bracket[3] = (int) (max * 0.9);
		bracket[4] = max;
		return bracket;
	}
	
	@Override
	public int getMax() {
		return max;
	}
	
	private int maxPoints() {
		System.out.println("MAxpoint is running");
		int total = 0;
		for(String word : possibleWords) {
			boolean invalid = false;
			if(hasCenter(word)) {
				boolean[] hasLetters = new boolean[word.length()];

				String[] wordarr = word.split("");

				int counterl = 0;
				//checks each letter of the word, and puts true to the values associated with the letter inside the boolean array when the letter is composed of the choice letters
				for(String wletter : wordarr) {
					for(String lletter : letters) {
						if(wletter.contentEquals(lletter)) {
							hasLetters[counterl] = true;
						}
					}
					counterl++;
				}
				for(boolean check : hasLetters) {
					if(!check) {
						invalid = true;
					}
				}
				if(!invalid) {
					if (word.length() == 4) {
						total = total + 1;
					}
					else if(word.length() > 4) {
						total = total + word.length();	
					}
					
					if(this.isPangram(word)) {
						total = total + 7;	
					}
				}
			}
		}
		return total;
	}
	public boolean isPangram(String word) {
		//boolean array, each one for the letter choices
		boolean[] pangram = new boolean[7];

		String[] wordarr = word.split("");

		for(String wletter : wordarr) {
			int counterl = 0;
			for(String lletter : letters) {
				//for each fixed letter, check if a letter from the word matches, if yes then set the boolean in one of the positions in the array to true.
				if(lletter.contentEquals(wletter)) {
					pangram[counterl] = true;
				}
				counterl++;
			}
		}
		//if there is at least one false value in the array, then the word is not a pangram
		for(boolean letterin : pangram) {
			if(!letterin) {
				return false;
			}
		}
		return true;
	
	}
	public boolean isValid(String word) {
		
		if(word.length() == 0) {
			errormessage = "Invalid: No Word";
			return false;
		}
		
		if(!(possibleWords.contains(word))) {
			errormessage = "Invalid: Not Found";
			return false;
		}
		
		//boolean array, each one for the letter choices
		boolean[] hasLetters = new boolean[word.length()];

		String[] wordarr = word.split("");

		int counterl = 0;
		//checks each letter of the word, and puts true to the values associated with the letter inside the boolean array when the letter is composed of the choice letters
		for(String wletter : wordarr) {
			for(String lletter : letters) {
				if(wletter.contentEquals(lletter)) {
					hasLetters[counterl] = true;
				}
			}
			counterl++;
		}
		
		//if the word has at least 1 unauthorized letter, the word becomes invalid
		for(boolean letterin : hasLetters) {
			if(!letterin) {
				errormessage = "Invalid: Bad Letter";
				return false;
			}
		}
		
		//if word does not contain center letter
		if(!(this.hasCenter(word))) {
			errormessage = "Invalid: No Center";
			return false;
		}
		
		if(wordFound.contains(word)) {
			errormessage = "Invalid: Used Word";
			return false;
		}

		return true;
	
	}
	public void addWord(String word) {
		wordFound.add(word);
	}
	public boolean hasCenter(String word) {
		String [] wordarr = word.split("");
		
		for(String letter : wordarr) {
			char charletter = letter.charAt(0);
			if(charletter == this.center) {
				return true;
			}
		}
		return false;
		
	}
	
	public boolean isUnique(String word) {
		int counter = 0;
		char a = 'a';
		word = word.toLowerCase();
		String[] letter = word.split("");			
		for(int i =0;i<26;i++) {
			for(int p =0;p<word.length();p++) {
				char charLetter = letter[p].charAt(0);
				if(a == charLetter) {
					counter++;
					break;
				}
			}
			a = (char)(a + 1);
		}
		if(counter == 7) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void splitLetters(String word) {
		int counter = 0;
		String[] str = new String[7];
		char a = 'a';
		word = word.toLowerCase();
		String[] letter = word.split("");			
		for(int i =0;i<26;i++) {
			for(int p =0;p<word.length();p++) {
				char charLetter = letter[p].charAt(0);
				if(a == charLetter) {
					String temp = ""+a;
					str[counter] = temp;
					counter++;
					break;
				}
			}
			a = (char)(a + 1);
		}
		letters = str;
	}
	/*public static void main(String[] args) {
		SpellingBeeGame a = new SpellingBeeGame();
		//SpellingBeeGame b = new SpellingBeeGame("abcdefg");
		//System.out.println(a.getAllLetters());
		String word = "Bashfull";
		//System.out.println(a.isUnique(word));
		//System.out.println(a.getAllLetters());
		System.out.println(a.getMax());
	}*/
}

