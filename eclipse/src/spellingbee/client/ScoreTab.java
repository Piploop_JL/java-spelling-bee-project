//Thomas Comeau 1934037, Jimmy Le 1936415
package spellingbee.client;
import spellingbee.network.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.client.*;

public class ScoreTab extends Tab{
	private Client app;
	private Text queenBee;
	private Text genius;
	private Text amazing;
	private Text good;
	private Text gettingStarted;
	private Text currentScore;
	private Text bee;
	private Text gen;
	private Text ama;
	private Text goo;
	private Text start;
	private Text scor;
	private String bracket;
	private int score;
	private int max;
	private String brack2;
	private String brack3;
	private String brack4;
	private String brack5;
	private TextField points;
	
	public ScoreTab(Client app) {
		super("Score");
		this.app = app;
		this.points = points;
		
		
		bracket = this.app.sendAndWaitMessage("getMax");
		max = Integer.parseInt(bracket);
		bracket = this.app.sendAndWaitMessage("getScore");
		score = Integer.parseInt(bracket);
		setBrackets();
		
		
		GridPane gridPane = new GridPane();
		queenBee = new Text(""+max);
		genius = new Text(brack5);
		amazing = new Text(brack4);
		good = new Text(brack3);
		gettingStarted = new Text(brack2);
		currentScore = new Text(bracket);
		bee = new Text("Queen Bee");
		gen = new Text("Genius");
		ama = new Text("Amazing");
		start = new Text("Getting Started");
		scor = new Text("Current Score");
		goo = new Text("Good");
		
		
		
		gridPane.add(queenBee, 1, 0);
		gridPane.add(bee, 0, 0);
		gridPane.add(genius, 1, 1);
		gridPane.add(gen, 0, 1);
		gridPane.add(amazing, 1, 2);
		gridPane.add(ama, 0, 2);
		gridPane.add(good, 1, 3);
		gridPane.add(goo, 0, 3);
		gridPane.add(gettingStarted, 1, 4);
		gridPane.add(start, 0, 4);
		gridPane.add(currentScore, 1, 5);
		gridPane.add(scor, 0, 5);
		
		gridPane.setHgap(10);
		
		bee.setFill(Color.GREY);
		gen.setFill(Color.GREY);
		ama.setFill(Color.GREY);
		start.setFill(Color.GREY);
		goo.setFill(Color.GREY);
		currentScore.setFill(Color.RED);
		this.setContent(gridPane);
	}
	
	public void setBrackets() {
		brack2 = "" + max/4;
		brack3 = "" + max/2;
		brack4 = "" + (int)(max * 0.75);
		brack5 = "" + (int)(max * 0.9);
	}
	public void refresh() {
		bracket = this.app.sendAndWaitMessage("getScore");
		score = Integer.parseInt(bracket);
		int temp = Integer.parseInt(brack2);
		currentScore.setText(bracket);
		if(score > temp) {
			start.setFill(Color.BLACK);
			temp = Integer.parseInt(brack3);
		}
		else if(score > temp) {
			goo.setFill(Color.BLACK);
			temp = Integer.parseInt(brack4);
		}
		else if(score > temp) {
			ama.setFill(Color.BLACK);
			temp = Integer.parseInt(brack5);
		}
		else if(score > temp) {
			gen.setFill(Color.BLACK);
			temp = max;
		}
		else if(score > temp) {
			bee.setFill(Color.BLACK);
		}
	}
	public Text getScore() {
		return scor;
	}
}