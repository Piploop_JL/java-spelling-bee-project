//Thomas Comeau 1934037, Jimmy Le 1936415
package spellingbee.client;
import spellingbee.network.*;

import javax.swing.event.ChangeEvent;
import javafx.beans.value.ChangeListener;

import ButtonEvents.ClearEvent;
import ButtonEvents.DeleteEvent;
import ButtonEvents.LetterEvent;
import ButtonEvents.SubmitEvent;
import javafx.scene.paint.*;
import javafx.scene.layout.*;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;


/**
 * setting up the format of the game tab to display on the client
 * @author jayja
 *
 */
public class GameTab extends Tab{
	private Client client;
	private Button[] buttonList;
	private Button delete;
	private Button clear;
	private TextField wordInput;
	private Button submit;
	private TextField message;
	private TextField points;
	private String allLetters;
	private ScoreTab score;

	/**
	 * constructor
	 * creates all the buttons, boxes and everything visible to the user
	 * @param client
	 */
	public GameTab(Client client, ScoreTab scoretab) {
		super("Game");
		this.client = client;
		this.score = scoretab;
		
		
		allLetters = this.client.sendAndWaitMessage("getAllLetters");
		String center =  this.client.sendAndWaitMessage("getCenterLetter");
		String[] splitLetters = allLetters.split("");
		
		
		//buttons used to manipulate the letters and word	
		buttonList = new Button[7];
		
		
		for(int j = 0; j < splitLetters.length; j++) {
			buttonList[j] = new Button(splitLetters[j]);
			if(splitLetters[j].contentEquals(center)) {
				buttonList[j].setTextFill(Color.DEEPPINK);
			}
		}
		
		submit = new Button("Submit");
		//lets get the basics to work, then il do these ones
		delete = new Button("Delete");
		clear = new Button("Clear All");
		
		
		//textbox where the letters inputed will appear
		
		wordInput = new TextField();
		
		message = new TextField("Welcome");
		
		points = new TextField("0");
		
		//Events
		//setting an event for all the letters
		
		for(int i = 0; i < splitLetters.length; i++) {
			LetterEvent le = new LetterEvent(splitLetters[i], wordInput);
			buttonList[i].setOnAction(le);
		}
		
		//submit event
		SubmitEvent se = new SubmitEvent(wordInput, message, points, this.client, score);
		submit.setOnAction(se);
		
		/*points.textProperty().addListener(new ChangeListener<String>(){
			private Tab gameScore = scoretab;
			

			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				// TODO Auto-generated method stub
				((ScoreTab) scoretab).refresh();
			}
		});*/
		
		//delete event
		DeleteEvent de = new DeleteEvent(wordInput);
		delete.setOnAction(de);
		
		//clear event
		ClearEvent ce = new ClearEvent(wordInput);
		clear.setOnAction(ce);
		
		//boxes
		
		VBox everything = new VBox();

		HBox letterButtons = new HBox();
		letterButtons.getChildren().addAll(buttonList[0],buttonList[1],buttonList[2],buttonList[3],buttonList[4],buttonList[5],buttonList[6]);
		
		HBox controlButtons = new HBox();
		controlButtons.getChildren().addAll(submit,delete,clear);
		
		HBox displays = new HBox();
		displays.getChildren().addAll(message,points);
		
		//grouping everything together in a box
		everything.getChildren().addAll(letterButtons,wordInput,controlButtons,displays);
		
		//putting everything onto the client
		this.setContent(everything);
		
	}
	
	/**
	 * returns all the letter choices
	 * @return
	 */
	public String getLetters() {
		return allLetters;
	}
	public TextField getScoreField() {
		return points;
	}
	
}
