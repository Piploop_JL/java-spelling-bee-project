//Thomas Comeau 1934037, Jimmy Le 1936415
package spellingbee.client;
import java.util.*;


/**
 * SimpleSpellingBeeGame used to test out the gui for the real thing
 * @author jimmy
 *
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	
	private String[] letters;
	private char center;
	private int points;
	private ArrayList<String> usedWords = new ArrayList<String>();
	private String errorMsg = "";
	
	/**
	 * Constructor for the SimpleSpellingBeeGame object.
	 * creates the fixed letters used as the possible choices.
	 * initializes the points.
	 * manually chooses a letter to be the center letter.
	 */
	public SimpleSpellingBeeGame() {
		letters = new String[7];
		letters[0] = "b";
		letters[1] = "a";
		letters[2] = "s";
		letters[3] = "h";
		letters[4] = "f";
		letters[5] = "u";
		letters[6] = "l";
		
		points = 0;
		center = 'a';
		
	}

	/**
	 * finds the amount of points a word is worth
	 * calls isValid to check if the word is valid or not. If it is invalid, the word is worth 0 points.
	 * calls isPangram to check if the word is a pangram. If it is one, add a bonus 7 points to the word.
	 * calls addWord to a list of words already used by the user, making it invalid the next time the user tries to use it.
	 * sums the point together to get the total points.
	 * return the amount of points that single word is worth.
	 */
	@Override
	public int getPointsForWord(String attempt) {
		int wordpoint = 0;
		//check if the word is valid
		if(isValid(attempt)) {
			if (attempt.length() == 4) {
				wordpoint = 1;
			}
			else if(attempt.length() > 4) {
				wordpoint = attempt.length();	
			}
			//check if the word is a pangram
			if(this.isPangram(attempt)) {
				wordpoint = wordpoint + 7;	
			}
			//adds the word to a list
			addWord(attempt);
		}
		//sums the points
		points = points + wordpoint;

		//returns the worth of the current word
		return wordpoint;
	}

	/**
	 * gives the message that should be displayed based on the word's validity.
	 */
	@Override
	public String getMessage(String attempt) {
		//if the word is valid
		if(this.isValid(attempt)) {
			
			return "Great!";
		}
		//if the word is invalid
		else {
			return errorMsg;
		}

	}

	/**
	 * returns all the letter choices possible
	 */
	@Override
	public String getAllLetters() {
		// TODO Auto-generated method stub
		String allletters = letters[0] + letters[1] + letters[2]+ letters[3]+ letters[4]+letters[5]+letters[6];
		return allletters;
	}

	/**
	 * returns the center letter
	 */
	@Override
	public char getCenterLetter() {
		return center;
	}

	/**
	 * returns the total amount of points
	 */
	@Override
	public int getScore() {
		return points;
	}

	/**
	 * i think these are used as milestones for the points (?)
	 */
	@Override
	public int[] getBrackets() {
		
		int[] brackets = new int[5];
		brackets[0] = 10;
		brackets[1] = 20;
		brackets[2] = 30;
		brackets[3] = 40;
		brackets[4] = 50;
		return brackets;
	}
	
	
	/**
	 * checks if the word is a pangram
	 * @param word
	 * @return
	 */
	public boolean isPangram(String word) {
		//boolean array, each one for the letter choices
		boolean[] pangram = new boolean[7];

		String[] wordarr = word.split("");

	
		for(String wletter : wordarr) {
			int counterl = 0;
			for(String lletter : letters) {
				//for each fixed letter, check if a letter from the word matches, if yes then set the boolean in one of the positions in the array to true.
				if(lletter.contentEquals(wletter)) {
					pangram[counterl] = true;
				}
				counterl++;
			}
		}
		//if there is at least one false value in the array, then the word is not a pangram
		for(boolean letterin : pangram) {
			if(!letterin) {
				return false;
			}
		}
		return true;
	
	}
	
	/**
	 * Checks through multiple conditions to make sure the word is valid
	 * @param word
	 * @return boolean
	 */
	public boolean isValid(String word) {
		
		//if there is no word = invalid
		if(word.length() == 0) {
			errorMsg = "Invalid: No Word";
			return false;
		}

		
		//boolean array, each one for the letter choices
		boolean[] hasLetters = new boolean[word.length()];

		String[] wordarr = word.split("");

		int counterl = 0;
		//checks each letter of the word, and puts true to the values associated with the letter inside the boolean array when the letter is composed of the choice letters
		for(String wletter : wordarr) {
			for(String lletter : letters) {
				if(wletter.contentEquals(lletter)) {
					hasLetters[counterl] = true;
				}
			}
			counterl++;
		}
		
		//if the word has at least 1 unauthorized letter, the word becomes invalid
		for(boolean letterin : hasLetters) {
			if(!letterin) {
				errorMsg = "Invalid: Bad Letter";
				return false;
			}
		}
		
		//if word does not contain center letter = invalid
		if(!(this.hasCenter(word))) {
			errorMsg = "Invalid: No Center";
			return false;
		}
		//if the word has already been used = invalid
		if(usedWords.contains(word)) {
			errorMsg = "Invalid: Used Word";
			return false;
		}

		//if none of the conditions above are met, the word is valid
		return true;
	
	}
	
	/**
	 * checks if the word has the center letter included.
	 * @param word
	 * @return boolean
	 */
	public boolean hasCenter(String word) {
		String [] wordarr = word.split("");
		
		//loops through the word array and compares the letters with the value of center
		for(String letter : wordarr) {
			char charletter = letter.charAt(0);
			if(charletter == this.center) {
				return true;
			}
		}
		return false;
		
	}
	//add the word into an arraylist of already used words
	public void addWord(String word) {
		usedWords.add(word);
	}

	@Override
	public int getMax() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/*
	 * used for testing.
	public static void main(String[] args) {
		SimpleSpellingBeeGame a = new SimpleSpellingBeeGame();
		String word = "bashful";
		System.out.println(a.isValid(word));
		System.out.println(a.isValid(word));
		
	}*/

	
}











