//Thomas Comeau 1934037, Jimmy Le 1936415
package spellingbee.client;

public interface ISpellingBeeGame {

	int getPointsForWord(String attempt);
	
	String getMessage(String attempt);
	
	String getAllLetters();
	
	char getCenterLetter();
	
	int getScore();
	
	int[] getBrackets();

	int getMax();
}
