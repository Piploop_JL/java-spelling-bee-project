//Thomas Comeau 1934037, Jimmy Le 1936415
//It works without the scoreTab but we could not figure out how to get it working with scoretab
package spellingbee.client;
import spellingbee.network.*;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;


public class SpellingBeeClient extends Application{
	private TextField points;

	
	@Override
	public void start(Stage stage) throws Exception {
		
		//client
		ClientApp ca = new ClientApp();
		Client client = ca.getClient();

		//gui
		Group root = new Group();	
		TabPane tp = new TabPane();
		
		//gametab
		Tab scoretab = new ScoreTab(client);
		Tab gametab = new GameTab(client, (ScoreTab)scoretab);
		tp.getTabs().add(gametab);
		tp.getTabs().add(scoretab);
		
		root.getChildren().add(tp);
		
		//scene
		Scene scene = new Scene(root,650,300);
		//scene.setFill(Color.STEELBLUE);
		
		//stage
		stage.setTitle("Spelling Bee!");
		stage.setScene(scene);
		stage.show();
		points = ((GameTab) gametab).getScoreField();
		
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}

}
