//Thomas Comeau 1934037, Jimmy Le 1936415
package ButtonEvents;

import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;

/**
 * Event used for the Clear button
 * clears out the text box
 * @author jimmy
 *
 */
public class ClearEvent implements EventHandler<ActionEvent>{
	private TextField input;
	
	public ClearEvent(TextField input) {
		
		this.input = input;

	}

	/**
	 * sets the text field to an empty string
	 */
	@Override
	public void handle(ActionEvent event) {
		input.setText("");
	}
}
