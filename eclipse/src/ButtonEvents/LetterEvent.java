//Thomas Comeau 1934037, Jimmy Le 1936415
package ButtonEvents;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

/**
 * event used for the letter buttons
 * @author jayja
 *
 */
public class LetterEvent implements EventHandler<ActionEvent>{
	
	private String letter;
	private TextField textfield;

	//constructor, takes as input the letter that it will represent
	public LetterEvent(String letter, TextField textfield) {
		this.letter = letter;
		this.textfield = textfield;
	}
	
	/**
	 * appends the letter to the text field
	 */
	@Override
	public void handle(ActionEvent e) {
		
		String text = textfield.getText();
		textfield.setText(text + letter);
		
	}

	
}
