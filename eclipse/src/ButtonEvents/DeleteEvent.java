//Thomas Comeau 1934037, Jimmy Le 1936415
package ButtonEvents;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
/**
 * the event used for the delete button
 * deletes the last letter in the text box
 * @author jimmy
 *
 */
public class DeleteEvent implements EventHandler<ActionEvent>{
		
	private TextField input;
	
	//constructor
	public DeleteEvent(TextField input) {
		
		this.input = input;

	}

	/**
	 * takes the value in the text box, and take away the last letter 
	 * sets the value of the text box to the new value produced.
	 */
	@Override
	public void handle(ActionEvent event) {
		String currentInput = input.getText();
		String[] currInputArr = currentInput.split("");
		String newInput = "";
		for(int i = 0; i < (currInputArr.length - 1); i++) {
			newInput = newInput + currInputArr[i];
		}
		input.setText(newInput);
		
		
	}
}
