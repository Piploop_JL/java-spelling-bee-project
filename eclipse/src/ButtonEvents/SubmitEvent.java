//Thomas Comeau 1934037, Jimmy Le 1936415
package ButtonEvents;
import spellingbee.client.ScoreTab;
import spellingbee.network.*;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

/**
 * event used for the submit button
 * sends the word to the server controller
 * updates the score and text field to the new values.
 * @author jimmy
 *
 */
public class SubmitEvent implements EventHandler<ActionEvent>{
	
	private TextField input;
	private TextField message;
	private TextField points;
	private Client client;
	private ScoreTab score;
	
	//constructor
	public SubmitEvent(TextField input, TextField message, TextField points, Client client, ScoreTab score) {
		
		this.input = input;
		this.message = message;
		this.points = points;
		this.client = client;
		this.score = score;
	}

	/**
	 * sends the word inside the text field to the server controller
	 * recieves and set the validity message
	 * recieve and update the total points.
	 */
	@Override
	public void handle(ActionEvent event) {
		String word = input.getText();
		//should be "good" / "bad"
		String result = client.sendAndWaitMessage("getMessage;"+word);
		message.setText(result);
		//hopefully if the serverside stuff validates well, the score should go up if its valid, and stay the same if it aint
		String totalPoints = client.sendAndWaitMessage("getScore;"+word);
		points.setText(totalPoints);
		score.getScore().setText(totalPoints);
		score.refresh();
	}
	
}
